import random
import datetime
from faker import Faker
from src.enums.datatype import Datatype
import logging

logger = logging.getLogger("name")


last_date = datetime.date(2000, 1, 1)


def gen_single_data_point(col, potential_id, fake, datatype):
    if datatype == "":
        if col[1] == "int":
            if (
                col[0].upper() == "ID"
                or col[0].upper() == "PNR"
                or col[0].upper() == "HNR"
                or col[0].upper() == "ABTNR"
                or col[0][-2:] == "Nr"
            ):
                return potential_id, Datatype.id
            if col[0].upper() == "COST" or col[0].upper() == "KOSTEN":
                return random.randint(50, 1_000_000), Datatype.very_large
            if col[0].upper() == "PLZ" or col[0].upper() == "ZIP":
                return fake.postcode(), Datatype.plz
            if col[0].upper() == "HAUSNUMMER":
                return random.randint(1, 125), Datatype.house_number
            return random.randint(0, 1000), Datatype.randinteger
        if col[1] == "varchar":
            if col[0].upper() == "EMAIL":
                return fake.email(), Datatype.email
            if col[0].upper() == "JOB" or col[0].upper() == "ARBEIT":
                return fake.job(), Datatype.job
            if col[0].upper() == "LAND" or col[0].upper() == "COUNTRY":
                return fake.country(), Datatype.country
            if col[0].upper() == "STREET" or col[0].upper() == "STREET":
                return fake.street(), Datatype.street
            if (
                col[0].upper() == "COMPANY"
                or col[0].upper() == "UNTERNEHMEN"
                or col[0].upper() == "FIRMA"
            ):
                return fake.company(), Datatype.company
            if (
                col[0].upper() == "LASTNAME"
                or col[0].upper() == "NAME"
                or col[0].upper == "NACHNAME"
            ):
                return fake.last_name(), Datatype.lastname
            if col[0].upper() == "FIRSTNAME" or col[0].upper() == "VORNAME":
                return fake.first_name(), Datatype.firstname
            if col[0].upper() == "ADDRESS" or col[0].upper() == "ADRESSE":
                return fake.address(), Datatype.address
            if (
                col[0].upper() == "DEPARTMENT"
                or col[0].upper() == "ABTEILUNG"
                or col[0].upper() == "ABTNAME"
            ):
                return generate_dept(), Datatype.dept
            if (
                col[0].upper() == "CITY"
                or col[0].upper() == "TOWN"
                or col[0].upper() == "VILLAGE"
                or col[0].upper() == "STADT"
                or col[0].upper() == "ORT"
            ):
                return fake.city(), Datatype.city
            if col[0].upper() == "PLZ" or col[0].upper() == "ZIP":
                return fake.postcode(), Datatype.plz
            if col[0].upper() == "PRODUKT" or col[0].upper() == "PRODUCT":
                return generate_product(), Datatype.product
            return fake.name(), Datatype.lastname
        if col[1] == "decimal":
            if col[0].upper() == "SALARY" or col[0].upper() == "GEHALT":
                return generate_salary(1000.0, 250_000.0), Datatype.salary
            if col[0].upper() == "BUDGET":
                return generate_salary(10000.0, 2_500_000.0), Datatype.humungous
            if col[0].upper() == "COST" or col[0].upper() == "KOSTEN":
                return generate_salary(50_000.0, 250_000.0), Datatype.salary
            return generate_salary(100.0, 100_000_000.0), Datatype.salary
        if col[1] == "date":
            if col[0].upper() == "BIRTHDATE" or col[0].upper() == "GEBURTSTAG":
                return fake.date_of_birth(), Datatype.birthdate
            if "BEGIN" in col[0].upper() or "START" in col[0].upper():
                date = fake.date_between()
                last_date = date
                return date, Datatype.date
            if ("ENDE" in col[0].upper() or "END" in col[0].upper()) and (
                col[1] == "date" or datatype == "Datum"
            ):
                return fake.date_between(last_date), Datatype.date
            return (
                fake.date_between(
                    datetime.date(1950, 1, 1), datetime.date(2020, 12, 31)
                ),
                Datatype.date,
            )
    else:
        if col[1] == "int":
            if datatype == "ID":
                return potential_id, Datatype.id
            if datatype == "Hausnummer":
                return random.randint(1, 125), Datatype.house_number
            if datatype == "PLZ":
                return fake.postcode(), Datatype.plz
            if datatype == "Gehalt":
                return random.randint(20_000, 250_000), Datatype.salary
            if datatype == "Budget":
                return random.randint(100_000, 1_000_000), Datatype.very_large
            if datatype == "Zufällige Zahl":
                return random.randint(0, 1_000_000), Datatype.randinteger
            if datatype == "1-10":
                return random.randint(1, 10), Datatype.small_number
            if datatype == "1-100":
                return random.randint(1, 100), Datatype.medium_small
            if datatype == "1-1.000":
                return random.randint(1, 1_000), Datatype.medium
            if datatype == "100.000-1.000.000":
                return random.randint(100_000, 1_000_000), Datatype.very_large
            if datatype == "10.000-100.000":
                return random.randint(10_000, 100_000), Datatype.medium_large
            if datatype == "1-10.000":
                return random.randint(1, 10_000), Datatype.medium_medium
            if datatype == "100.000-2.500.000":
                return random.randint(100_000, 2_500_000), Datatype.humungous
        if col[1] == "varchar":
            if datatype == "Straße":
                return fake.street(), Datatype.street
            if datatype == "Email":
                return fake.email(), Datatype.email
            if datatype == "Arbeit":
                return fake.job(), Datatype.job
            if datatype == "Nachname":
                return fake.last_name(), Datatype.lastname
            if datatype == "Vorname":
                return fake.first_name(), Datatype.firstname
            if datatype == "Adresse":
                return fake.address(), Datatype.address
            if datatype == "Ortsname":
                return fake.city(), Datatype.city
            if datatype == "Abteilung":
                return generate_dept(), Datatype.dept
            if datatype == "Land":
                return fake.country(), Datatype.country
            if datatype == "PLZ":
                return fake.postcode(), Datatype.plz
            if datatype == "Produkt":
                return generate_product(), Datatype.product
            return fake.last_name(), Datatype.lastname
        if col[1] == "decimal":
            if datatype == "Gehalt":
                return generate_salary(20_000.0, 250_000.0), Datatype.salary
            if datatype == "Budget":
                return generate_salary(100_000.0, 1_000_000.0), Datatype.very_large
            if datatype == "Kosten":
                return generate_salary(100_000.0, 1_000_000.0), Datatype.very_large
            if datatype == "Zufällige Zahl":
                return generate_salary(0.0, 1_000_000.0), Datatype.randinteger
            if datatype == "1-10":
                return generate_salary(1.0, 10.0), Datatype.small_number
            if datatype == "1-100":
                return generate_salary(1.0, 100.0), Datatype.medium_small
            if datatype == "1-1.000":
                return generate_salary(1.0, 1_000.0), Datatype.medium
            if datatype == "100.000-1.000.000":
                return generate_salary(100_000.0, 1_000_000.0), Datatype.very_large
            if datatype == "10.000-100.000":
                return generate_salary(10_000.0, 100_000.0), Datatype.medium_large
            if datatype == "1.0-10.000":
                return generate_salary(1.0, 10_000.0), Datatype.medium_medium
            if datatype == "100.000-2.500.000":
                return generate_salary(100_000.0, 2_500_000.0), Datatype.humungous
            return generate_salary(1.0, 1000.0), Datatype.randfloat
        if col[1] == "date":
            if col[0].upper() == "BIRTHDATE" or col[0].upper() == "GEBURTSTAG":
                return fake.date_of_birth(), Datatype.birthdate
            if "BEGIN" in col[0].upper() or "START" in col[0].upper():
                date = fake.date_between()
                last_date = date
                return date, Datatype.date
            if ("ENDE" in col[0].upper() or "END" in col[0].upper()) and (
                col[1] == "date" or datatype == "Datum"
            ):
                return fake.date_between(last_date), Datatype.date
            return (
                fake.date_between(
                    datetime.date(1950, 1, 1), datetime.date(2020, 12, 31)
                ),
                Datatype.date,
            )


def generate_product():
    product_list = [
        "Hammer",
        "Schraubenzieher",
        "Dübel",
        "Werkzeugkiste",
        "Bohrer",
        "Engländer",
        "Öl",
        "Schrauben",
        "Vorschlaghammer",
        "Pinsel",
        "Besen",
    ]
    index = random.randint(0, len(product_list) - 1)
    return product_list[index]


def gen_single_data_point_v2(col, potential_id, fake, datatype):
    if (
        (
            col[0].upper() == "ID"
            or col[0].upper() == "PNR"
            or col[0].upper() == "HNR"
            or col[0].upper() == "ABTNR"
            or col[0][-2:] == "Nr"
        )
        and col[1] == "int"
    ) or datatype == "ID":
        return potential_id, Datatype.id
    if (
        col[0].upper() == "LASTNAME"
        or col[0].upper() == "NAME"
        or col[0].upper == "NACHNAME"
    ) or datatype == "Nachname":
        return fake.last_name(), Datatype.lastname
    if (
        col[0].upper() == "FIRSTNAME" or col[0].upper() == "VORNAME"
    ) or datatype == "Vorname":
        return fake.first_name(), Datatype.firstname
    if (
        col[0].upper() == "BIRTHDATE" or col[0].upper() == "GEBURTSTAG"
    ) or datatype == "Geburtstag":
        return fake.date_of_birth(), Datatype.birthdate
    if (
        col[0].upper() == "ADDRESS" or col[0].upper() == "ADDRESSE"
    ) or datatype == "Adresse":
        return fake.address(), Datatype.address
    if (
        col[0].upper() == "DEPARTMENT"
        or col[0].upper() == "ABTEILUNG"
        or col[0].upper() == "ABTNAME"
    ) or datatype == "Abteilung":
        return generate_dept(), Datatype.dept
    if (
        (col[0].upper() == "SALARY" or col[0].upper() == "GEHALT")
        and col[1] == "decimal"
    ) or datatype == "Gehalt":
        return generate_salary(1000.0, 250_000.0), Datatype.salary
    if (col[0].upper() == "BUDGET") or datatype == "Budget":
        return generate_salary(10000.0, 2_500_000.0), Datatype.budget
    if (col[0].upper() == "KATEGORIE" or datatype == "Kategorie") and col[
        1
    ] == "varchar":
        return generate_category(), Datatype.category
    if (col[0].upper() == "KATEGORIE" or datatype == "Kategorie") and col[1] == "int":
        return random.randint(1, 10), Datatype.randinteger
    if ("BEGIN" in col[0].upper() or "START" in col[0].upper()) and (
        col[1] == "date" or datatype == "Datum"
    ):
        date = fake.date_between()
        last_date = date
        return date, Datatype.date
    if ("ENDE" in col[0].upper() or "END" in col[0].upper()) and (
        col[1] == "date" or datatype == "Datum"
    ):
        return fake.date_between(last_date), Datatype.date
    if (
        (col[0].upper() == "COST" or col[0].upper() == "KOSTEN") or datatype == "Kosten"
    ) and col[1] == "decimal":
        return generate_salary(50.0, 1_000_000.0), Datatype.cost
    if (
        (col[0].upper() == "COST" or col[0].upper() == "KOSTEN") or datatype == "Kosten"
    ) and col[1] == "int":
        return random.randint(50, 1_000_000), Datatype.cost
    if (
        (
            col[0].upper() == "CITY"
            or col[0].upper() == "TOWN"
            or col[0].upper() == "VILLAGE"
            or col[0].upper() == "STADT"
            or col[0].upper() == "ORT"
        )
        or datatype == "Ortsname"
    ) and col[1] == "varchar":
        return fake.city(), Datatype.city
    if (
        (col[0].upper() == "PLZ" or col[0].upper() == "ZIP" and datatype == "")
        or datatype == "PLZ"
    ) and col[1] == "int":
        return fake.postcode(), Datatype.plz
    if col[1] == "int":
        return random.randint(0, 1000), Datatype.randinteger
    if col[1] == "varchar":
        return fake.name(), Datatype.lastname
    if col[1] == "decimal":
        return generate_salary(100.0, 100_000_000.0), Datatype.salary
    if col[1] == "date":
        return (
            fake.date_between(datetime.date(1950, 1, 1), datetime.date(2020, 12, 31)),
            Datatype.date,
        )


def gen_unique_data(
    data_to_generate, potential_id, fake, table_name, column_name, col, unallowed
):
    result, _ = gen_specific_data(
        data_to_generate, potential_id, fake, table_name, column_name, col
    )
    while result in unallowed:
        result, _ = gen_specific_data(
            data_to_generate, potential_id, fake, table_name, column_name, col
        )
    return result, 0


def gen_specific_data(
    data_to_generate, potential_id, fake, table_name, column_name, col
):
    datatype_to_generate = ""
    for entry in data_to_generate:
        if entry[0] == table_name and entry[1] == column_name:
            datatype_to_generate = entry[2]
            break
    if data_to_generate != "":
        new_tuple = []
        new_tuple.append("")
        new_tuple.append(col[1])
        col = new_tuple
    result, _ = gen_single_data_point(col, potential_id, fake, datatype_to_generate)
    return result, 0


def get_generation_information(col):
    result = []
    fake = Faker("de_DE")
    for column in col:
        value, datatype = gen_single_data_point(column, 1, fake, "")
        result.append(datatype)
    return result


def generate_dept():
    depts = [
        "Verwaltung",
        "Produktion",
        "HR",
        "Verkauf",
        "Vertrieb",
        "Entwicklung",
        "Vorstand",
        "Aufsichtsrat",
    ]
    return depts[random.randint(0, len(depts) - 1)]


def generate_salary(lower_limit, upper_limit):
    return random.uniform(lower_limit, upper_limit)


def generate_category():
    list_of_categories = ["Business", "Family", "Wellness"]
    return list_of_categories[random.randint(0, len(list_of_categories) - 1)]
